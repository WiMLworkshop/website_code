FROM node:12

WORKDIR /code
#Install prod dependencies only
ARG NODE_ENV=production
COPY package.json yarn.lock lerna.json ./
COPY packages/api/package.json packages/api/package.json
RUN yarn install --frozen-lockfile

# Copy only dist and node_modules, very light docker
FROM node:12-alpine
WORKDIR /code
COPY ./packages/reactapp/build ./packages/reactapp/build
COPY ./packages/api/dist ./packages/api/dist
COPY --from=0 /code/node_modules ./node_modules
COPY --from=0 /code/packages/api/node_modules ./packages/api/node_modules
# Don't need client node modules
#COPY --from=0 /code/packages/client/node_modules ./packages/client/node_modules
# Need env.example file for validation
COPY .env.example ./
EXPOSE 8080
CMD ["node", "packages/api/dist/src/main.js"]
