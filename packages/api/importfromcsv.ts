import { createConnection } from 'typeorm-fixtures-cli/dist'
import csv = require('csv-parser')
import { getRepository } from 'typeorm'
import { join } from 'path'
import { CognitoIdentityServiceProvider } from 'aws-sdk'
import { UserType } from 'aws-sdk/clients/cognitoidentityserviceprovider'
import * as fs from 'fs'
import {PositionTypeEntity, RoleEntity, UserEntity} from './src/entities'
import { parse } from 'date-fns'
import * as config from './ormconfig'

require('dotenv-safe').config({
  path: join(__dirname, '../../.env'),
  example: join(__dirname, '../../.env.example'),
})

const emailRegexp = RegExp(
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
)

type UserRow = {
  timestamp: Date
  name: string
  position: string
  affiliation: string
  keywords?: string
  website?: string
  email: string
  public: boolean
  mentor: boolean
}

const positionTypeMap: Record<string, string> = {
  'Ph.D. Student': 'PhD student',
  'Data Scientist': 'Researcher / (Data) Scientist / Engineer',
  'Researcher': 'Researcher / (Data) Scientist / Engineer',
  'Masters Student': 'Undergraduate or Master student',
  'Postdoc': 'Post-doctoral researcher',
  'Software Engineer': 'Researcher / (Data) Scientist / Engineer',
  'Undergraduate': 'Undergraduate or Master student',
}

const loadUsersFromFile = (
  filePath: string,
): Promise<{ [email: string]: UserRow }> =>
  new Promise((resolve, reject) => {
    console.log(`Reading CSV file at ${filePath}`)
    const results: any[] = []
    fs.createReadStream(filePath)
      .pipe(csv())
      .on('data', (data: any) => results.push(data))
      .on('end', () => {
        console.log(`Found ${results.length} rows.`)
        const groupedByEmail = results.reduce<{ [email: string]: UserRow }>(
          function(r, a) {
            a.email = a.email ? a.email.toLowerCase() : undefined
            if (a.email && emailRegexp.test(a.email)) {
              a.timestamp = parse(a.timestamp, 'M/d/yyyy H:mm:ss', new Date())
              if (!r[a.email] || r[a.email].timestamp < a.timestamp) {
                r[a.email] = {
                  ...a,
                  public: !!(a.public === 'Yes'),
                  mentor: !!(a.mentor === 'Yes'),
                }
              }
            }
            return r
          },
          Object.create(null),
        )
        console.log(
          `Found ${Object.keys(groupedByEmail).length} unique user emails.`,
        )
        resolve(groupedByEmail)
      })
      .on('error', (err: Error) => {
        reject(err)
      })
  })

const createCognitoUser = async (email: string): Promise<UserType> => {
  const cognito = new CognitoIdentityServiceProvider({
    region: process.env.AWS_REGION!,
  })
  return (
    await cognito
      .adminCreateUser({
        UserPoolId: process.env.COGNITO_USER_POOL_ID!,
        Username: email,
        UserAttributes: [
          {
            Name: 'email',
            Value: email,
          },
          {
            Name: 'email_verified',
            Value: 'true',
          },
        ],
        // Enable in case fails and needs to be run again
        // MessageAction: 'RESEND',
      })
      .promise()
  ).User!
}

const importMembersFromCsv = async (csvPath: string) => {
  console.log('Importing Users from CSV file')
  let connection

  try {
    console.log('Connecting and synchronizing DB')
    connection = await createConnection(config, 'default')
    const userRepository = getRepository(UserEntity)
    const rolesRepository = getRepository(RoleEntity)
    const positionTypesRepository = getRepository(PositionTypeEntity)
    console.log('DB sync is complete')
    const userRows = await loadUsersFromFile(csvPath)
    const entities = []
    const positionTypes = await positionTypesRepository.find()
    const userRole = await rolesRepository.findOne({ name: 'User' })
    const adminRole = await rolesRepository.findOne({ name: 'Admin' })
    if (!userRole || !adminRole) {
      throw new Error('"User" and "Admin" roles dont exist in DB.')
    }
    for (const email of Object.keys(userRows)) {
      const cognitoUser = await createCognitoUser(email)
      const userRow = userRows[email]
      const nameArray = userRow.name
        ? userRow.name.split(' ')
        : ['', '']
      const rawPositionName = positionTypeMap[userRow.position]
      const positionType: PositionTypeEntity | undefined = rawPositionName
        ? positionTypes.find(pt => pt.name === rawPositionName)
        : undefined
      const keywords = userRow.keywords
        ? userRow.keywords.length > 512
          ? userRow.keywords.substr(0, 511)
          : userRow.keywords
        : undefined
      const affiliation = userRow.affiliation
        ? userRow.affiliation.length > 255
          ? userRow.affiliation.substr(0, 254)
          : userRow.affiliation
        : undefined
      entities.push({
        email,
        role: email === 'jessicaschrouff@gmail.com'
          ? adminRole
          : userRole,
        positionType,
        externalId: cognitoUser.Username!,
        firstName: nameArray.slice(0, nameArray.length - 1).join(' '),
        lastName: nameArray[nameArray.length - 1],
        affiliation,
        public: userRow.public,
        verified: false,
        website: userRow.website,
        keywords,
        mentor: userRow.mentor,
        createdAt: userRow.timestamp,
      })
    }
    await userRepository.save(userRepository.create(entities))
  } catch (err) {
    throw err
  } finally {
    if (connection) {
      await connection.close()
    }
  }
}

importMembersFromCsv('./data.csv')
  .catch(reason => console.error(reason))
  .then(() => {
    console.log('Done importing')
    return process.exit()
  })
