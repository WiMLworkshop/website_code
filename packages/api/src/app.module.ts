import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ConfigModule } from './config/config.module'
import { ConfigService } from './config/config.service'
import {CountryEntity, FieldOfResearchEntity, PositionTypeEntity, RoleEntity, UserEntity} from './entities'
import { UsersModule } from './users/users.module'
import { AuthModule } from './auth/auth.module'
import { AwsModule } from './aws/aws.module'
import { join } from 'path'
import { ServeStaticModule } from '@nestjs/serve-static'
import { CountriesModule } from './countries/countries.module'
import {PositionTypesModule} from './positionTypes/positiontype.module'
import {FieldsOfResearchModule} from './fieldsOfResearch/fieldofresearch.module'

@Module({
  imports: [
    AwsModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (config: ConfigService) => ({
        type: 'mysql',
        host: config.mysqlHost,
        username: config.mysqlUser,
        password: config.mysqlPassword,
        database: config.mysqlDbName,
        synchronize: true,
        entities: [
          UserEntity,
          CountryEntity,
          RoleEntity,
          PositionTypeEntity,
          FieldOfResearchEntity
        ],
      }),
      inject: [ConfigService],
    }),
    AuthModule,
    UsersModule,
    CountriesModule,
    FieldsOfResearchModule,
    PositionTypesModule,
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '../../../', 'reactapp', 'build'),
    }),
  ],
})
export class AppModule {}
