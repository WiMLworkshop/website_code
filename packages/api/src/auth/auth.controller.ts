import {
  Controller,
  Post,
  UseGuards,
  Res,
  Body,
  Req,
  UnauthorizedException,
  NotFoundException,
} from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { Response, Request } from 'express'
import {
  ChangePasswordDto,
  ConfirmPasswordResetDto,
  ConfirmSignupDto,
  LoginDto,
  RefreshTokenDto,
  ResendConfirmationEmailDto,
  ResetPasswordDto,
  SetPasswordDto,
  SignupRequestDto,
} from './auth.types'
import {CountryEntity, FieldOfResearchEntity, PositionTypeEntity, UserEntity} from '../entities'
import { AuthService } from './auth.service'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { RolesService } from '../roles/roles.service'
import { UsersService } from '../users/users.service'
// import { AWSError } from 'aws-sdk'
const AWSError = require('aws-sdk')

@Controller('auth')
export class AuthController {
  constructor(
    @InjectRepository(CountryEntity)
    private readonly countriesRepo: Repository<CountryEntity>,
    @InjectRepository(PositionTypeEntity)
    private readonly positionsRepo: Repository<PositionTypeEntity>,
    @InjectRepository(FieldOfResearchEntity)
    private readonly fieldsRepo: Repository<FieldOfResearchEntity>,
    private readonly authService: AuthService,
    private readonly rolesService: RolesService,
    private readonly usersService: UsersService,
  ) {}

  @Post('login')
  async login(@Body() { email, password }: LoginDto, @Res() res: Response) {
    const result = await this.authService.login(email, password)
    res.status(200).send(result)
  }

  @Post('set-password')
  async setPassword(
    @Body() { session, ...userData }: SetPasswordDto,
    @Res() res: Response,
  ) {
    const result = await this.authService.setPassword(session, userData.email, userData.password)
    let country: CountryEntity | undefined
    let positionType: PositionTypeEntity | undefined
    let fields: FieldOfResearchEntity[] = []
    if (userData.countryId) {
      country = await this.countriesRepo.findOne(userData.countryId)
      if (!country) {
        throw new NotFoundException(
          `Country with id ${userData.countryId} is not found for user signup.`,
        )
      }
    }
    if (userData.positionTypeId) {
      positionType = await this.positionsRepo.findOne(userData.positionTypeId)
      if (!positionType) {
        throw new NotFoundException(
          `Position Type with id ${userData.positionTypeId} is not found for user signup.`,
        )
      }
    }
    if (userData.fields) {
      fields = await this.fieldsRepo.findByIds(userData.fields)
    }
    const updatedUser = await this.usersService.update(result.user!.id, {
      ...result.user!,
      ...userData,
      country,
      positionType,
      fields,
      verified: true
    })
    res.status(200).send({ ...result, user: updatedUser })
  }

  @Post('refresh')
  async refresh(
    @Body() { refreshToken }: RefreshTokenDto,
    @Res() res: Response,
  ) {
    const result = await this.authService.refreshToken(refreshToken)
    res.status(200).send(result)
  }

  @Post('reset')
  async reset(@Body() { email }: ResetPasswordDto, @Res() res: Response) {
    await this.authService.resetPassword(email)
    res.status(204).send()
  }

  @Post('confirm-reset')
  async confirmReset(
    @Body() { email, password, code }: ConfirmPasswordResetDto,
    @Res() res: Response,
  ) {
    const result = await this.authService.confirmResetPassword(
      email,
      password,
      code,
    )
    res.status(200).send(result)
  }

  @Post('signup')
  async signup(@Body() data: SignupRequestDto, @Res() res: Response) {
    try {
      const cognitoUser = await this.authService.signUp(data)
      const role = await this.rolesService.userRole()
      let country: CountryEntity | undefined
      let positionType: PositionTypeEntity | undefined
      let fields: FieldOfResearchEntity[] = []
      if (data.countryId) {
        country = await this.countriesRepo.findOne(data.countryId)
        if (!country) {
          throw new NotFoundException(
            `Country with id ${data.countryId} is not found for user signup.`,
          )
        }
      }
      if (data.positionTypeId) {
        positionType = await this.positionsRepo.findOne(data.positionTypeId)
        if (!positionType) {
          throw new NotFoundException(
            `Position Type with id ${data.positionTypeId} is not found for user signup.`,
          )
        }
      }
      if (data.fields) {
        fields = await this.fieldsRepo.findByIds(data.fields)
      }
      await this.usersService.create({
        ...data,
        email: data.email,
        externalId: cognitoUser.externalId!,
        country,
        positionType,
        fields,
        role,
      })
      res.status(204).send()
    } catch (e) {
      console.error(e)
      if (typeof e == AWSError && e.code === 'UsernameExistsException') {
        await this.authService.resendConfirmationCode(data.email)
        res.status(204).send()
      } else {
        await this.usersService.delete(data.email)
      }
      res.status(400).send()
    }
  }

  @Post('resend-confirmation')
  async resendConfirmationEmail(
    @Body() { email }: ResendConfirmationEmailDto,
    @Res() res: Response,
  ) {
    const destinationEmail = await this.authService.resendConfirmationCode(
      email,
    )
    res.status(204).send({ email: destinationEmail })
  }

  @Post('confirm-signup')
  async confirmSignup(
    @Body() { email, password, code }: ConfirmSignupDto,
    @Res() res: Response,
  ) {
    const user = await this.usersService.findOne({ email })
    if (!user) {
      throw new UnauthorizedException()
    }
    await this.authService.confirmSignUp(email, password, code)
    await this.usersService.update(user.id, { ...user, verified: true })
    const result = await this.authService.login(email, password)
    res.status(200).send(result)
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('change-password')
  async changePassword(
    @Req() req: Request & { user: UserEntity },
    @Body() { currentPassword, newPassword }: ChangePasswordDto,
    @Res() res: Response,
  ) {
    await this.authService.changePassword(
      req.user.email,
      currentPassword,
      newPassword,
    )
    res.status(204).send()
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('logout')
  async logout(@Req() req: Request, @Res() res: Response) {
    const header = req!.header('Authorization')
    if (!header || header.split(' ').length !== 2) {
      throw new UnauthorizedException()
    }
    const accessToken = header.split(' ')[1]
    await this.authService.logout(accessToken)
    res.status(204).send()
  }
}
