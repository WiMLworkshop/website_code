import { UsersModule } from '../users/users.module'
import { PassportModule } from '@nestjs/passport'
import { HttpModule, Module } from '@nestjs/common'
import { JwtStrategy } from './jwt.strategy'
import { AuthController } from './auth.controller'
import { AwsModule } from '../aws/aws.module'
import { AuthService } from './auth.service'
import { RolesModule } from '../roles/roles.module'
import { TypeOrmModule } from '@nestjs/typeorm'
import {CountryEntity, FieldOfResearchEntity, PositionTypeEntity} from '../entities'
import {PositionTypesModule} from '../positionTypes/positiontype.module'
import {FieldsOfResearchModule} from '../fieldsOfResearch/fieldofresearch.module'

@Module({
  imports: [
    AwsModule,
    TypeOrmModule.forFeature([CountryEntity, FieldOfResearchEntity, PositionTypeEntity]),
    RolesModule,
    UsersModule,
    PositionTypesModule,
    FieldsOfResearchModule,
    PassportModule,
    HttpModule,
  ],
  providers: [AuthService, AuthController, JwtStrategy],
  controllers: [AuthController],
})
export class AuthModule {}
