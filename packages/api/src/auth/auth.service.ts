import { Inject, Injectable, UnauthorizedException } from '@nestjs/common'
import { UsersService } from '../users/users.service'
import {
  AuthChallenge,
  ChallengeName,
  LoginResult,
  Session,
  SignupRequestDto,
} from './auth.types'
import { getRole, RoleType, UserEntity } from '../entities'
import { AWS_COGNITO } from '../aws/aws.module'
import { ConfigService } from '../config/config.service'
import * as CognitoIdentityServiceProvider from 'aws-sdk/clients/cognitoidentityserviceprovider'
import {
  AdminRespondToAuthChallengeRequest,
  AuthenticationResultType,
  ChallengeNameType,
  ChallengeParametersType,
  SessionType,
} from 'aws-sdk/clients/cognitoidentityserviceprovider'

@Injectable()
export class AuthService {
  private readonly poolId: string
  private readonly clientId: string

  constructor(
    @Inject(AWS_COGNITO)
    private readonly cognito: CognitoIdentityServiceProvider,
    private readonly usersService: UsersService,
    private readonly config: ConfigService,
  ) {
    this.poolId = config.cognitoUserPoolId
    this.clientId = config.cognitoClientId
  }

  async login(email: string, password: string): Promise<LoginResult> {
    try {
      const data = await this.cognito
        .adminInitiateAuth({
          AuthFlow: 'ADMIN_NO_SRP_AUTH',
          ClientId: this.clientId,
          UserPoolId: this.poolId,
          AuthParameters: {
            USERNAME: email,
            PASSWORD: password,
          },
        })
        .promise()
      const user = await this.usersService.findOne({ email })
      return this.processLoginResult(data, user)
    } catch (e) {
      console.error(e)
      throw new UnauthorizedException('Incorrect email or password')
    }
  }

  async setPassword(
    session: string,
    userName: string,
    password: string,
  ): Promise<LoginResult> {
    const data = await this.cognito
      .adminRespondToAuthChallenge(<AdminRespondToAuthChallengeRequest>{
        ClientId: this.clientId,
        UserPoolId: this.poolId,
        ChallengeName: 'NEW_PASSWORD_REQUIRED',
        Session: session,
        ChallengeResponses: {
          NEW_PASSWORD: password,
          USERNAME: userName,
        },
      })
      .promise()
    const user = await this.usersService.findOne({ email: userName })
    return this.processLoginResult(data, { ...user, verified: true })
  }

  async refreshToken(token: string): Promise<Session> {
    const data = await this.cognito
      .adminInitiateAuth({
        AuthFlow: 'REFRESH_TOKEN',
        ClientId: this.clientId,
        UserPoolId: this.poolId,
        AuthParameters: {
          REFRESH_TOKEN: token,
        },
      })
      .promise()
    const session = <Session>{}
    if (data.AuthenticationResult) {
      session.token = data.AuthenticationResult.IdToken
      session.accessToken = data.AuthenticationResult.AccessToken
      session.expiresIn = data.AuthenticationResult.ExpiresIn
    }
    return session
  }

  async logout(token: string): Promise<void> {
    await this.cognito.globalSignOut({ AccessToken: token }).promise()
    return Promise.resolve()
  }

  async resetPassword(email: string): Promise<void> {
    await this.cognito
      .forgotPassword({
        ClientId: this.clientId,
        Username: email,
      })
      .promise()
    return Promise.resolve()
  }

  async confirmResetPassword(
    email: string,
    password: string,
    code: string,
  ): Promise<LoginResult> {
    await this.cognito
      .confirmForgotPassword({
        ClientId: this.clientId,
        ConfirmationCode: code,
        Username: email,
        Password: password,
      })
      .promise()
    return this.login(email, password)
  }

  async changePassword(
    email: string,
    currentPassword: string,
    newPassword: string,
  ): Promise<void> {
    await this.login(email, currentPassword)
    await this.cognito
      .adminSetUserPassword({
        UserPoolId: this.poolId,
        Username: email,
        Password: newPassword,
        Permanent: true,
      })
      .promise()
  }

  async signUp({
    email,
    password,
  }: SignupRequestDto): Promise<Partial<UserEntity>> {
    const result = await this.cognito
      .signUp({
        ClientId: this.clientId,
        Username: email,
        Password: password,
      })
      .promise()
    return { externalId: result.UserSub }
  }

  async resendConfirmationCode(email: string): Promise<string> {
    const result = await this.cognito
      .resendConfirmationCode({
        ClientId: this.clientId,
        Username: email,
      })
      .promise()
    if (
      !result.CodeDeliveryDetails ||
      !result.CodeDeliveryDetails.Destination
    ) {
      throw new UnauthorizedException('Missing send confirmation')
    }
    return result.CodeDeliveryDetails.Destination
  }

  async confirmSignUp(
    email: string,
    password: string,
    code: string,
  ): Promise<void> {
    await this.cognito
      .confirmSignUp({
        ClientId: this.clientId,
        Username: email,
        ConfirmationCode: code,
      })
      .promise()
  }

  async validateUserWithExternalId(
    externalId: string,
  ): Promise<Partial<UserEntity> | undefined> {
    const user = await this.cognito
      .adminGetUser({
        UserPoolId: this.poolId,
        Username: externalId,
      })
      .promise()
    if (!user.Enabled || user.UserStatus !== 'CONFIRMED') {
      throw new UnauthorizedException()
    }
    return { externalId: user.Username! }
  }

  private async processLoginResult(data: {
    Session?: SessionType
    AuthenticationResult?: AuthenticationResultType
    ChallengeName?: ChallengeNameType
    ChallengeParameters?: ChallengeParametersType
  }, user: UserEntity): Promise<LoginResult> {
    let session: Session | null = null
    let challenge: AuthChallenge | null = null
    let role = getRole(user.role)
    if (data.AuthenticationResult) {
      session = {}
      session.token = data.AuthenticationResult.IdToken
      session.accessToken = data.AuthenticationResult.AccessToken
      session.refreshToken = data.AuthenticationResult.RefreshToken
      session.expiresIn = data.AuthenticationResult.ExpiresIn

      // User hasn't verified their email address
      if (session.accessToken && !user.verified) {
        await this.resendConfirmationCode(user.email)
        session = {}
        challenge = {
          challengeName: ChallengeName.SIGN_UP_CONFIRMATION_REQUIRED,
          session: '',
          userName: user.externalId,
        }
      }
    }

    // User was imported and needs to set new password.
    if (data.ChallengeName) {
      if (!Object.values(ChallengeName as any).includes(data.ChallengeName)) {
        throw new Error(`Unknown login challenge ${data.ChallengeName}`)
      }
      challenge = {
        challengeName: data.ChallengeName as ChallengeName,
        session: data.Session!,
        userName: data.ChallengeParameters!['USER_ID_FOR_SRP'],
      }
    }
    return { session, challenge, role, user } as LoginResult
  }
}
