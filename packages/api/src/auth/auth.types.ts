import {
  IsBoolean,
  IsEmail,
  IsJWT,
  IsNotEmpty,
  IsOptional,
  IsPositive,
  MinLength,
} from 'class-validator'
import {RoleType, UserEntity} from '../entities'

export enum ChallengeName {
  NEW_PASSWORD_REQUIRED = 'NEW_PASSWORD_REQUIRED',
  SIGN_UP_CONFIRMATION_REQUIRED = 'SIGN_UP_CONFIRMATION_REQUIRED',
}

export type AuthChallenge = {
  session: string
  challengeName: ChallengeName
  userName: string
}

export type Session = {
  token?: string
  accessToken?: string
  refreshToken?: string
  expiresIn?: number
}

export type LoginResult = {
  session?: Session
  challenge?: AuthChallenge
  role?: RoleType
  user?: UserEntity
}

export class LoginDto {
  @IsEmail()
  readonly email: string

  @IsNotEmpty()
  readonly password: string
}

export class RefreshTokenDto {
  @IsNotEmpty()
  readonly refreshToken: string
}

export class ResetPasswordDto {
  @IsEmail()
  readonly email: string
}

export class ConfirmPasswordResetDto {
  @IsEmail()
  readonly email: string

  @MinLength(8)
  readonly password: string

  @IsNotEmpty()
  readonly code: string
}

export class SignupRequestDto {
  @IsEmail()
  readonly email: string

  @MinLength(8)
  readonly password: string

  @IsNotEmpty()
  readonly firstName: string

  @IsNotEmpty()
  readonly lastName: string

  @IsOptional()
  readonly positionTypeId?: number

  @IsNotEmpty()
  readonly fields: number[]

  @IsOptional()
  readonly affiliation?: string

  @IsOptional()
  readonly public?: boolean

  @IsOptional()
  readonly mentor?: boolean

  @IsOptional()
  readonly city?: string

  @IsOptional()
  readonly countryId?: number

  @IsOptional()
  readonly keywords?: string

  @IsOptional()
  readonly website?: string
}

export class ResendConfirmationEmailDto {
  @IsEmail()
  readonly email: string
}

export class SetPasswordDto extends SignupRequestDto {
  @IsNotEmpty()
  readonly session: string
}

export class ChangePasswordDto {
  @IsNotEmpty()
  readonly currentPassword: string

  @MinLength(8)
  readonly newPassword: string
}

export class ConfirmSignupDto {
  @IsEmail()
  readonly email: string

  @MinLength(8)
  readonly password: string

  @IsNotEmpty()
  readonly code: string
}
