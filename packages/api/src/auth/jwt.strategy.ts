import { ExtractJwt, Strategy } from 'passport-jwt'
import { HttpService, Injectable, UnauthorizedException } from '@nestjs/common'
import { PassportStrategy } from '@nestjs/passport'
import { ConfigService } from '../config/config.service'
import jwt from 'jsonwebtoken'
import jwkToPem from 'jwk-to-pem'
import { AuthService } from './auth.service'
import { UsersService } from '../users/users.service'

export type CognitoJwk = jwkToPem.JWK & {
  alg: string
  kid: string
  use: string
}

let cognitoJwk: CognitoJwk[] | undefined

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly config: ConfigService,
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
    private readonly httpService: HttpService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKeyProvider: async (
        req: Request,
        jwtToken: string,
        done: any,
      ) => {
        const region = config.awsRegion
        const userPoolId = config.cognitoUserPoolId
        const decoded = jwt.decode(jwtToken, { complete: true, json: true })
        if (!decoded || !decoded['header'] || !decoded['header'].kid) {
          return done(new Error('Failed to decode jwtToken'))
        }
        if (cognitoJwk === undefined) {
          const config = await httpService
            .get<{ keys: CognitoJwk[] }>(
              `https://cognito-idp.${region}.amazonaws.com/${userPoolId}/.well-known/jwks.json`,
            )
            .toPromise()
          cognitoJwk = config.data.keys
        }
        const key = cognitoJwk.find(key => key.kid === decoded['header'].kid)
        if (!key) {
          return done(new Error('Missing PEM key for jwtToken'))
        }
        done(null, jwkToPem(key))
      },
    })
  }

  async validate(payload: any) {
    const user = await this.authService.validateUserWithExternalId(payload.sub)
    if (!user) {
      throw new UnauthorizedException()
    }
    return this.usersService.findOne({ externalId: user.externalId! })
  }
}
