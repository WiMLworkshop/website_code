import { Global, Module, Provider } from '@nestjs/common'
import { CognitoIdentityServiceProvider, SES } from 'aws-sdk'
import { ConfigService } from '../config/config.service'

export const AWS_COGNITO = 'AWS_COGNITO'
export const AWS_SES = 'AWS_SES'

export const cognitoProvider: Provider = {
  provide: AWS_COGNITO,
  useFactory: (config: ConfigService) => {
    return new CognitoIdentityServiceProvider({ region: config.awsRegion })
  },
  inject: [ConfigService],
}

export const sesProvider: Provider = {
  provide: AWS_SES,
  useFactory: (config: ConfigService) => {
    return new SES({ apiVersion: '2010-12-01', region: config.awsRegion })
  },
  inject: [ConfigService],
}

@Global()
@Module({
  providers: [cognitoProvider, sesProvider],
  exports: [cognitoProvider, sesProvider],
})
export class AwsModule {}
