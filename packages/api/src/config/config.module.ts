import { Global, Module } from '@nestjs/common'
import { ConfigService } from './config.service'
import { join } from 'path'

@Global()
@Module({
  providers: [ConfigService],
  exports: [ConfigService],
})
export class ConfigModule {
  constructor() {
    ConfigModule.loadEnv()
  }

  private static loadEnv(): void {
    require('dotenv-safe').config({
      path: join(__dirname, '../../../../../.env'),
      example: join(__dirname, '../../../../../.env.example'),
      allowEmptyValues: true,
    })
  }
}
