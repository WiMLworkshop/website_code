import { Injectable } from '@nestjs/common'
import { version as packageVersion } from '../../package.json'

type EnvType = string | boolean | number

function assertIsDefined<T>(
  val: T,
  name: string,
): asserts val is NonNullable<T> {
  if (val === undefined || val === null) {
    throw new Error(`Expected ${name} to be defined, but received ${val}`)
  }
}

@Injectable()
export class ConfigService {
  get nodeEnv(): string {
    return this.get<string>('NODE_ENV')
  }

  get version(): string {
    return packageVersion as string
  }

  get isDevelopment(): boolean {
    return this.nodeEnv === 'development'
  }

  get clientOrigin(): string {
    return this.get<string>('CLIENT_ORIGIN')
  }

  get port(): number {
    return process.env.PORT ? parseInt(process.env.PORT) : 3000
  }

  get mysqlHost(): string {
    return this.get<string>('MYSQL_HOST')
  }

  get mysqlUser(): string {
    return this.get<string>('MYSQL_USER')
  }

  get mysqlPassword(): string {
    return this.get<string>('MYSQL_PASSWORD')
  }

  get mysqlDbName(): string {
    return this.get<string>('MYSQL_DATABASE')
  }

  get awsRegion(): string {
    return this.get<string>('AWS_REGION')
  }

  get cognitoUserPoolId(): string {
    return this.get<string>('COGNITO_USER_POOL_ID')
  }

  get cognitoClientId(): string {
    return this.get<string>('COGNITO_CLIENT_ID')
  }

  get sourceEmail(): string {
    return this.get<string>('SOURCE_EMAIL')
  }

  protected get<T extends EnvType>(name: string): T {
    const val = process.env[name]
    assertIsDefined(val, name)
    return val as T
  }
}
