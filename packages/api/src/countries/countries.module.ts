import { Injectable, Module } from '@nestjs/common'
import { InjectRepository, TypeOrmModule } from '@nestjs/typeorm'
import { CountryEntity } from '../entities'
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm'
import { Repository } from 'typeorm'
import { Crud } from '@nestjsx/crud'
import { Controller } from '@nestjs/common'

@Injectable()
export class CountriesCrudService extends TypeOrmCrudService<CountryEntity> {
  constructor(
    @InjectRepository(CountryEntity) repo: Repository<CountryEntity>,
  ) {
    super(repo)
  }
}

@Crud({
  model: {
    type: CountryEntity,
  },
  routes: {
    only: ['getManyBase', 'getOneBase'],
  },
  query: {
    allow: ['id', 'name'],
    alwaysPaginate: true,
  },
})
@Controller('countries')
class CountriesController {
  constructor(public service: CountriesCrudService) {}
}

@Module({
  imports: [TypeOrmModule.forFeature([CountryEntity])],
  providers: [CountriesCrudService],
  controllers: [CountriesController],
})
export class CountriesModule {}
