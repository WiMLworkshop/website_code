import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm'
import { UserEntity } from './user.entity'

@Entity({ name: 'Country' })
export class CountryEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  name: string

  @OneToMany(
    () => UserEntity,
    user => user.country,
  )
  users: UserEntity[]
}
