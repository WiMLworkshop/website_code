import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm'

@Entity({ name: 'FieldOfResearch' })
export class FieldOfResearchEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  name: string
}
