import {Entity, Column, PrimaryGeneratedColumn, OneToMany} from 'typeorm'
import {UserEntity} from './user.entity'

@Entity({ name: 'PositionType' })
export class PositionTypeEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  name: string

  @OneToMany(
    () => UserEntity,
    user => user.positionType,
  )
  users: UserEntity[]
}
