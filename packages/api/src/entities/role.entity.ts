import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm'
import { UserEntity } from './user.entity'

export enum RoleType {
  User = 'User',
  Admin = 'Admin',
}

export function getRole(entity: RoleEntity) {
  if (entity.name === RoleType.Admin) {
    return RoleType.Admin
  }
  return RoleType.User
}

@Entity({ name: 'Role' })
export class RoleEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  name: string

  @OneToMany(
    () => UserEntity,
    users => users.role,
  )
  users: UserEntity[]
}
