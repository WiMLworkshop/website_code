import {
  Entity,
  Column,
  JoinColumn,
  PrimaryGeneratedColumn,
  Index,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne, ManyToMany, JoinTable,
} from 'typeorm'
import { CountryEntity } from './country.entity'
import { RoleEntity } from './role.entity'
import {FieldOfResearchEntity} from './field.entity'
import {PositionTypeEntity} from './position.type.entity'

@Entity({ name: 'User' })
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  externalId: string

  @Column({ unique: true })
  email: string

  @Index()
  @Column()
  firstName: string

  @Index()
  @Column()
  lastName: string

  @ManyToOne(
    () => PositionTypeEntity,
    positionType => positionType.users,
    { nullable: true },
  )
  @JoinColumn({ name: 'positionTypeId' })
  positionType: PositionTypeEntity

  @Column({ nullable: true })
  positionTypeId: number

  @ManyToMany(type => FieldOfResearchEntity)
  @JoinTable()
  fields: FieldOfResearchEntity[]

  @Index()
  @Column({ nullable: true })
  affiliation: string

  @Column({ default: false })
  verified: boolean

  @Index()
  @Column({ nullable: true, length: 512 })
  keywords?: string

  @Column({ nullable: true })
  city?: string

  @Column({ nullable: true })
  website?: string

  @Column({ default: false })
  public: boolean

  @Column({ default: false })
  mentor: boolean

  @Column({ default: false })
  informAboutOpportunities: boolean

  @Column({ default: false })
  informAboutActivities: boolean

  @ManyToOne(
    () => CountryEntity,
    country => country.users,
    { nullable: true },
  )
  @JoinColumn({ name: 'countryId' })
  country: CountryEntity

  @Column({ nullable: true })
  countryId: number

  @ManyToOne(
    () => RoleEntity,
    role => role.users,
    { nullable: false },
  )
  @JoinColumn({ name: 'roleId' })
  role: RoleEntity

  @Column({ nullable: false })
  roleId: number

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date
}
