import { Injectable, Module } from '@nestjs/common'
import { InjectRepository, TypeOrmModule } from '@nestjs/typeorm'
import { FieldOfResearchEntity } from '../entities'
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm'
import { Repository } from 'typeorm'
import { Crud } from '@nestjsx/crud'
import { Controller } from '@nestjs/common'

@Injectable()
export class FieldsOfResearchCrudService extends TypeOrmCrudService<FieldOfResearchEntity> {
  constructor(
    @InjectRepository(FieldOfResearchEntity) repo: Repository<FieldOfResearchEntity>,
  ) {
    super(repo)
  }
}

@Crud({
  model: {
    type: FieldOfResearchEntity,
  },
  routes: {
    only: ['getManyBase', 'getOneBase'],
  },
  query: {
    allow: ['id', 'name'],
    alwaysPaginate: true,
  },
})
@Controller('research-fields')
class FieldsOfResearchController {
  constructor(public service: FieldsOfResearchCrudService) {}
}

@Module({
  imports: [TypeOrmModule.forFeature([FieldOfResearchEntity])],
  providers: [FieldsOfResearchCrudService],
  controllers: [FieldsOfResearchController],
})
export class FieldsOfResearchModule {}
