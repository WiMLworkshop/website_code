import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import rateLimit from 'express-rate-limit'
import { CorsOptions } from '@nestjs/common/interfaces/external/cors-options.interface'
import { ConfigService } from './config/config.service'
import { ValidationPipe } from '@nestjs/common'
//var cors = require('cors');

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  //app.use(cors());
  const config = app.get(ConfigService)
  const corsOptions: CorsOptions = {
    credentials: true,
    origin: config.clientOrigin,
    methods: 'GET, HEAD, PUT, PATCH, POST, DELETE, OPTIONS',
    preflightContinue: false,
    optionsSuccessStatus: 200,
    allowedHeaders: ['Content-Type', 'authorization'],
  }
  app.use(
    rateLimit({
      windowMs: 5 * 60 * 1000, // 5 minutes
      max: 10000, // limit each IP to 100 requests per windowMs
    }),
  )
  app.enableCors(corsOptions)
  app.useGlobalPipes(new ValidationPipe())
  console.log(`LISTENING ON PORT ${config.port}`)
  await app.listen(config.port)
}
bootstrap()
