import { Injectable, Module } from '@nestjs/common'
import { InjectRepository, TypeOrmModule } from '@nestjs/typeorm'
import { PositionTypeEntity } from '../entities'
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm'
import { Repository } from 'typeorm'
import { Crud } from '@nestjsx/crud'
import { Controller } from '@nestjs/common'

@Injectable()
export class PositionTypesCrudService extends TypeOrmCrudService<PositionTypeEntity> {
  constructor(
    @InjectRepository(PositionTypeEntity) repo: Repository<PositionTypeEntity>,
  ) {
    super(repo)
  }
}

@Crud({
  model: {
    type: PositionTypeEntity,
  },
  routes: {
    only: ['getManyBase', 'getOneBase'],
  },
  query: {
    allow: ['id', 'name'],
    alwaysPaginate: true,
  },
})
@Controller('positions')
class PositionTypesController {
  constructor(public service: PositionTypesCrudService) {}
}

@Module({
  imports: [TypeOrmModule.forFeature([PositionTypeEntity])],
  providers: [PositionTypesCrudService],
  controllers: [PositionTypesController],
})
export class PositionTypesModule {}
