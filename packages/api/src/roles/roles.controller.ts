import { Crud } from '@nestjsx/crud'
import { RoleEntity } from '../entities'
import { Controller, UseGuards } from '@nestjs/common'
import { RolesGuard } from './roles.guard'
import { RolesCrudService } from './roles.service'

@UseGuards(RolesGuard)
@Crud({
  model: {
    type: RoleEntity,
  },
  routes: {
    only: ['getManyBase', 'getOneBase'],
  },
  query: {
    allow: ['id', 'name'],
  },
})
@Controller('roles')
export class RolesController {
  constructor(public service: RolesCrudService) {}
}
