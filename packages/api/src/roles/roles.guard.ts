import { Injectable, UnauthorizedException } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { Reflector } from '@nestjs/core'
import { RoleEntity, RoleType } from '../entities'

@Injectable()
export class RolesGuard extends AuthGuard('jwt') {
  constructor(private readonly reflector: Reflector) {
    super()
  }

  handleRequest(err: any, user: any, info: any, context: any) {
    if (err || !user) {
      throw err || new UnauthorizedException()
    }
    const rolesHandler =
      this.reflector.get<RoleType[]>('roles', context.getHandler()) || []
    const rolesClass =
      this.reflector.get<RoleType[]>('roles', context.getClass()) || []
    const roles = [...rolesClass, ...rolesHandler]
    if (roles && !RolesGuard.rolesMatch(roles, user.role)) {
      new UnauthorizedException()
    }
    return user
  }

  static rolesMatch(roles: RoleType[], userRole: RoleEntity): boolean {
    return roles.find(roleName => userRole.name === roleName) !== undefined
  }
}
