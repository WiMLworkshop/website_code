import { Module } from '@nestjs/common'
import { RolesCrudService, RolesService } from './roles.service'
import { TypeOrmModule } from '@nestjs/typeorm'
import { RoleEntity } from '../entities'
import { RolesGuard } from './roles.guard'
import { RolesController } from './roles.controller'

@Module({
  imports: [TypeOrmModule.forFeature([RoleEntity])],
  providers: [RolesService, RolesCrudService, RolesGuard],
  controllers: [RolesController],
  exports: [RolesService, RolesGuard],
})
export class RolesModule {}
