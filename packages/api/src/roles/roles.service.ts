import { Injectable, NotFoundException } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { RoleEntity, RoleType } from '../entities'
import { Repository } from 'typeorm'
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm'

@Injectable()
export class RolesCrudService extends TypeOrmCrudService<RoleEntity> {
  constructor(@InjectRepository(RoleEntity) repo: Repository<RoleEntity>) {
    super(repo)
  }
}

@Injectable()
export class RolesService {
  constructor(
    @InjectRepository(RoleEntity)
    private readonly roleRepo: Repository<RoleEntity>,
  ) {}

  async userRole(): Promise<RoleEntity> {
    const role = await this.roleRepo.findOne({ name: RoleType.User })
    if (!role) {
      throw new NotFoundException('Role "User" is not found.')
    }
    return role
  }

  async adminRole(): Promise<RoleEntity> {
    const role = await this.roleRepo.findOne({ name: RoleType.Admin })
    if (!role) {
      throw new NotFoundException('Role "Admin" is not found.')
    }
    return role
  }
}
