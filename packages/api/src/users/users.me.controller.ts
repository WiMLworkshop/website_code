import {
  Crud,
  CrudAuth,
  CrudController,
  Override,
  ParsedBody,
} from '@nestjsx/crud'
import { UserEntity } from '../entities'
import { Controller, Req, UseGuards } from '@nestjs/common'
import { UsersCrudService, UsersService } from './users.service'
import { RolesGuard } from '../roles/roles.guard'
import { UpdateUserDto } from './users.types'

@UseGuards(RolesGuard)
@Crud({
  model: {
    type: UserEntity,
  },
  routes: {
    only: ['getOneBase', 'updateOneBase', 'deleteOneBase'],
  },
  params: {
    id: {
      primary: true,
      disabled: true,
    },
  },
  dto: {
    update: UpdateUserDto,
  },
  query: {
    exclude: ['id'],
    join: {
      country: {
        allow: [],
      },
      positionType: {
        eager: true,
      },
      fields: {
        eager: true,
      }
    },
  },
})
@CrudAuth({
  property: 'user',
  filter: (user: UserEntity) => ({
    id: user.id,
  }),
})
@Controller('profile/me')
export class MeUserController implements CrudController<UserEntity> {
  constructor(
    public service: UsersCrudService,
    private readonly usersService: UsersService,
  ) {}

  get base(): CrudController<UserEntity> {
    return this
  }

  @Override()
  async getOne(
    @Req() { user }: Request & { user: UserEntity },
    @ParsedBody() dto: UpdateUserDto,
  ) {
    return { ...user, id: 'me' }
  }

  @Override()
  async deleteOne(@Req() { user }: Request & { user: UserEntity }) {
    return this.usersService.delete(user.email)
  }

  @Override()
  async updateOne(
    @Req() { user }: Request & { user: UserEntity },
    @ParsedBody() dto: UpdateUserDto,
  ) {
    const updatedUser = await this.usersService.update(user.id, {
      ...dto,
      id: user.id,
      verified: user.verified,
      role: user.role,
    })
    return { ...updatedUser, id: 'me' }
  }
}
