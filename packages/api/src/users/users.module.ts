import { Module } from '@nestjs/common'
import { PublicUserController } from './users.public.controller'
import { PrivateUserController } from './users.private.controller'
import { UsersCrudService, UsersService } from './users.service'
import { TypeOrmModule } from '@nestjs/typeorm'
import { UserEntity } from '../entities'
import { MeUserController } from './users.me.controller'
import { RolesModule } from '../roles/roles.module'

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity]), RolesModule],
  providers: [UsersService, UsersCrudService],
  controllers: [PublicUserController, PrivateUserController, MeUserController],
  exports: [UsersService],
})
export class UsersModule {}
