import {
  Crud,
  CrudController,
  CrudRequest,
  Override,
  ParsedRequest,
} from '@nestjsx/crud'
import { UserEntity } from '../entities'
import { Controller, UseGuards } from '@nestjs/common'
import { UsersCrudService } from './users.service'
import { RolesGuard } from '../roles/roles.guard'
import { Roles } from '../roles/roles.decorator'
import { RoleType as UserRole } from '../entities'
import { mutateMultiValueSearchParams } from './users.util'

@UseGuards(RolesGuard)
@Roles(UserRole.Admin)
@Crud({
  model: {
    type: UserEntity,
  },
  routes: {
    exclude: ['createManyBase', 'createOneBase'],
    deleteOneBase: {
      returnDeleted: true
    },
  },
  query: {
    sort: [
      {
        field: 'lastName',
        order: 'DESC',
      },
      {
        field: 'firstName',
        order: 'DESC',
      },
    ],
    maxLimit: 100,
    alwaysPaginate: true,
    join: {
      country: {
        alias: 'uc',
      },
      role: {
        eager: true,
      },
      positionType: {
        eager: true,
      },
      fields: {
        eager: true,
      }
    },
  },
})
@Controller('private/users')
export class PrivateUserController implements CrudController<UserEntity> {
  constructor(public service: UsersCrudService) {}

  get base(): CrudController<UserEntity> {
    return this
  }

  @Override()
  getMany(@ParsedRequest() req: CrudRequest) {
    if (req.parsed.search.$and) {
      mutateMultiValueSearchParams(req)
    }
    return this.base.getManyBase!(req)
  }
}
