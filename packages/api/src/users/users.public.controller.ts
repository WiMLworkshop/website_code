import {
  Crud,
  CrudController,
  CrudRequest,
  Override,
  ParsedRequest,
} from '@nestjsx/crud'
import { UserEntity } from '../entities'
import { Controller } from '@nestjs/common'
import { UsersCrudService } from './users.service'
import { mutateMultiValueSearchParams } from './users.util'

@Crud({
  model: {
    type: UserEntity,
  },
  routes: {
    only: ['getManyBase', 'getOneBase'],
  },
  query: {
    allow: [
      'id',
      'firstName',
      'lastName',
      'positionType',
      'positionTypeId',
      'fields',
      'affiliation',
      'keywords',
      'website',
      'city',
      'country',
      'updatedAt',
    ],
    filter: {
      verified: {
        $eq: true,
      },
      public: {
        $eq: true,
      },
    },
    sort: [
      {
        field: 'lastName',
        order: 'DESC',
      },
      {
        field: 'firstName',
        order: 'DESC',
      },
    ],
    maxLimit: 100,
    alwaysPaginate: true,
    join: {
      country: {
        eager: true,
      },
      positionType: {
        eager: true,
      },
      fields: {
        eager: true,
      }
    },
  },
})
@Controller('public/users')
export class PublicUserController implements CrudController<UserEntity> {
  constructor(public service: UsersCrudService) {}

  get base(): CrudController<UserEntity> {
    return this
  }

  @Override()
  getMany(@ParsedRequest() req: CrudRequest) {
    if (req.parsed.search.$and) {
      mutateMultiValueSearchParams(req)
    }
    return this.base.getManyBase!(req)
  }
}
