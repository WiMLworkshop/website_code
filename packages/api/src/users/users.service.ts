import { Inject, Injectable, NotFoundException } from '@nestjs/common'
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm'
import { UserEntity } from '../entities'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { CreateUserDto, UpdateUserWithChangesDto } from './users.types'
import { AWS_COGNITO } from '../aws/aws.module'
import * as CognitoIdentityServiceProvider from 'aws-sdk/clients/cognitoidentityserviceprovider'
import { ConfigService } from '../config/config.service'

@Injectable()
export class UsersCrudService extends TypeOrmCrudService<UserEntity> {
  constructor(@InjectRepository(UserEntity) repo: Repository<UserEntity>) {
    super(repo)
  }
}

type FindById = { id: number }
type FindByExternalId = { externalId: string }
type FindByAccessToken = { accessToken: string }
type FindByEmail = { email: string }

type FindOneParams =
  | FindById
  | FindByExternalId
  | FindByEmail
  | FindByAccessToken

function isByAccessToken(params: FindOneParams): params is FindByAccessToken {
  return (params as FindByAccessToken).accessToken !== undefined
}

@Injectable()
export class UsersService {
  private readonly poolId: string
  private readonly clientId: string

  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    @Inject(AWS_COGNITO)
    private readonly cognito: CognitoIdentityServiceProvider,
    private readonly config: ConfigService,
  ) {
    this.poolId = config.cognitoUserPoolId
    this.clientId = config.cognitoClientId
  }

  async findOne(params: FindOneParams): Promise<UserEntity> {
    if (isByAccessToken(params)) {
      const cognitoUser = await this.cognito
        .getUser({
          AccessToken: params.accessToken,
        })
        .promise()
      return this.findOne({ externalId: cognitoUser.Username })
    } else {
      const user = await this.userRepository.findOne(params, {
        relations: ['role', 'country', 'positionType', 'fields'],
      })
      if (!user) {
        throw new NotFoundException(`User with params ${params} is not found.`)
      }
      return user
    }
  }

  async create(user: CreateUserDto): Promise<UserEntity> {
    return await this.userRepository.save(user)
  }

  async update(
    id: number,
    user: UpdateUserWithChangesDto,
  ): Promise<UserEntity> {
    const currentUser = await this.userRepository.findOne(id)
    if (!currentUser) {
      throw new NotFoundException()
    }
    if (currentUser && currentUser.email !== user.email) {
      await this.cognito
        .adminUpdateUserAttributes({
          UserPoolId: this.poolId,
          Username: user.email,
          UserAttributes: [
            {
              Name: 'email',
              Value: user.email,
            },
            {
              Name: 'email_verified',
              Value: 'true',
            },
          ],
        })
        .promise()
    }
    const updatedUser = this.userRepository.merge(currentUser, user)
    return this.userRepository.save(updatedUser)
  }

  async delete(email: string): Promise<boolean> {
    await this.cognito
      .adminDeleteUser({
        UserPoolId: this.poolId,
        Username: email,
      })
      .promise()
    await this.userRepository.delete({ email })
    console.log(`User with email ${email} is deleted.`)
    return true
  }
}
