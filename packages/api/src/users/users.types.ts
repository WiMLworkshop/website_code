import {
  IsNotEmpty,
  IsEmail,
  IsOptional,
  IsUUID,
  IsBoolean,
} from 'class-validator'
import {CountryEntity, FieldOfResearchEntity, PositionTypeEntity, RoleEntity} from '../entities'
import {ManyToOne} from 'typeorm'

export class CreateUserDto {
  @IsUUID()
  readonly externalId: string

  @IsEmail()
  readonly email: string

  @IsNotEmpty()
  readonly firstName: string

  @IsNotEmpty()
  readonly lastName: string

  @IsOptional()
  readonly affiliation?: string

  @IsOptional()
  readonly public?: boolean

  @IsOptional()
  readonly mentor?: boolean

  @IsOptional()
  readonly informAboutOpportunities?: boolean

  @IsOptional()
  readonly informAboutActivities?: boolean

  @IsOptional()
  readonly city?: string

  @IsOptional()
  readonly keywords?: string

  @IsOptional()
  readonly website?: string

  @IsOptional()
  @ManyToOne(
    () => PositionTypeEntity,
    positionType => positionType.users,
    { nullable: true },
  )
  readonly positionType?: PositionTypeEntity

  @IsOptional()
  readonly fields?: FieldOfResearchEntity[]

  @IsOptional()
  @ManyToOne(
    () => CountryEntity,
    country => country.users,
    { nullable: true },
  )
  readonly country?: CountryEntity

  @IsOptional()
  @ManyToOne(
    () => RoleEntity,
    role => role.users,
    { nullable: false },
  )
  readonly role: RoleEntity
}

export class UpdateUserDto {
  @IsNotEmpty()
  readonly id: number

  @IsEmail()
  readonly email: string

  @IsNotEmpty()
  readonly firstName: string

  @IsNotEmpty()
  readonly lastName: string

  @IsOptional()
  @ManyToOne(
    () => PositionTypeEntity,
    positionType => positionType.users,
    { nullable: true },
  )
  readonly positionType?: PositionTypeEntity

  @IsOptional()
  readonly fields?: FieldOfResearchEntity[]

  @IsOptional()
  readonly affiliation?: string

  @IsOptional()
  readonly public?: boolean

  @IsOptional()
  readonly mentor?: boolean

  @IsOptional()
  readonly informAboutOpportunities?: boolean

  @IsOptional()
  readonly informAboutActivities?: boolean

  @IsOptional()
  readonly city?: string

  @IsOptional()
  @ManyToOne(
    () => CountryEntity,
    country => country.users,
    { nullable: true },
  )
  readonly country?: CountryEntity

  @IsOptional()
  readonly keywords?: string

  @IsOptional()
  readonly website?: string
}

export class UpdateUserWithChangesDto extends UpdateUserDto {
  @IsBoolean()
  readonly verified: boolean

  @ManyToOne(
    () => RoleEntity,
    role => role.users,
    { nullable: false },
  )
  readonly role: RoleEntity
}
