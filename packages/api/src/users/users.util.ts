import { SFields } from '@nestjsx/crud-request'
import { CrudRequest } from '@nestjsx/crud'
import {split} from 'ts-node'

export const mutateMultiValueSearchParams = (req: CrudRequest) => {
  // Identify fields that are allowed to have comma separated values
  const fields = ['firstName', 'lastName', 'keywords', 'city', 'positionTypeId', 'fields.id']

  // Switch from $and to $or conditions
  const filters = req.parsed.search.$and!.filter(f => f !== undefined)
  const orSearch = req.parsed.search as SFields
  orSearch.$or = filters
  delete orSearch.$and

  // Identify which filters are included into every $or condition
  const singleValueFilters = Object.assign(
    {},
    ...filters.filter(f => Object.keys(f).every(k => !fields.includes(k))),
  )
  const multiValueFilters = filters.filter(f =>
    Object.keys(f).some(k => fields.includes(k)),
  )

  // Recursive function that makes sure all conditions are included
  // e.g. if there are two multiValue filters all of their values need to be included into every $or condition
  // {
  //  filter1: value1,
  //  filter2: value1,
  // },
  // {
  //  filter1: value1,
  //  filter2: value2,
  // },
  // {
  //  filter1: value2,
  //  filter2: value1,
  // },
  // {
  //  filter1: value2,
  //  filter2: value2,
  // },
  const buildConditions = (
    filters: any[],
    includedFilters: SFields[],
    allFilters: any[],
  ): any[] => {
    while (filters.length > 0) {
      const f: any = filters.pop()
      const fieldName = Object.keys(f)
        .filter(k => fields.includes(k))
        .pop()!
      const value = f[fieldName].cont || f[fieldName].eq
      const splitValue = value.split !== undefined ? value.split(',') : value
      if (splitValue.length > 1) {
        return allFilters.concat(
          splitValue.map((v: string) =>
            buildConditions(
              filters.slice(0),
              {
                ...includedFilters,
                [fieldName]: {
                  cont: v.trim(),
                },
              },
              allFilters,
            ),
          ),
        )
      } else {
        if (filters.length) {
          return allFilters.concat([
            buildConditions(
              filters.slice(0),
              {
                ...includedFilters,
                ...f,
              },
              allFilters,
            ),
          ])
        }
        return allFilters.concat([
          {
            ...includedFilters,
            ...f,
          },
        ])
      }
    }
    return includedFilters
  }

  const conditions = buildConditions(
    multiValueFilters.slice(0),
    singleValueFilters,
    [],
  )

  // Discard all $or conditions that don't include all of the filters
  orSearch.$or = []
    .concat(...(Array.isArray(conditions) ? conditions : [conditions]))
    .filter(c => {
      return (
        Object.keys(c).length ===
        multiValueFilters.length + Object.keys(singleValueFilters).length
      )
    })
}
