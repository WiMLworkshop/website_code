import React from 'react'
import { Admin, Resource } from 'react-admin'
import users from './Users'
import { API_URL } from './constants'
import Layout from './Layout'
import { AuthProvider, httpClient, PermissionsLevel } from './Auth'
import customRoutes from './customRoutes'
import crudProvider from './nestJsxCrud'

const dataProvider = crudProvider(API_URL!, httpClient)
const App = () => (
  <Admin
    layout={Layout}
    loginPage={false}
    title={'Women in Machine Learning Directory'}
    dataProvider={dataProvider}
    initialState={{ admin: { ui: { sidebarOpen: false } } }}
    customRoutes={customRoutes}
    authProvider={AuthProvider}
  >
    {(permissions: PermissionsLevel) => {
      return [
        <Resource name="private/users" {...users(permissions)} />,
        <Resource name="public/users" {...users(permissions)} />,
        <Resource name="countries" />,
        <Resource name="research-fields" />,
        <Resource name="positions" />,
        <Resource name="profile" />,
        <Resource name="roles" />,
      ]
    }}
  </Admin>
)

export default App
