import React, { Children } from 'react'
import { usePermissions, LoadingIndicator, useAuthProvider } from 'react-admin'
import { makeStyles } from '@material-ui/styles'
import {
  AppBar as MUIAppBar,
  Button,
  IconButton,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
} from '@material-ui/core'
import { AccountCircle, LockOpen, PersonAdd } from '@material-ui/icons'
import useMediaQuery from '@material-ui/core/useMediaQuery/useMediaQuery'
import { AuthProvider, PermissionsLevel } from './Auth'
import { withRouter } from 'react-router'

const useStyles = makeStyles(
  (theme: any) => ({
    toolbar: {
      padding: '0 24px',
    },
    title: {
      flex: 1,
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
    },
  }),
  { name: 'RaAppBar' },
)

const AppBarImpl = (props: any) => {
  const {
    children,
    classes: classesOverride,
    className,
    logo,
    open,
    title,
    userMenu,
    staticContext,
    ...rest
  } = props
  const classes = useStyles()
  const { permissions } = usePermissions()
  const authenticated = permissions !== PermissionsLevel.Guest
  const authProvider = useAuthProvider()
  const isXSmall = useMediaQuery((theme: any) => theme.breakpoints.down('xs'))

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const isMenuOpen = Boolean(anchorEl)

  const openMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const closeMenu = () => {
    setAnchorEl(null)
  }

  const handleLogout = () => {
    authProvider.logout()
    closeMenu()
    props.history.push('/login')
  }

  const handleStartLogin = () => {
    AuthProvider.enableLogin()
    props.history.push('/login')
  }

  const handleSignup = () => {
    AuthProvider.enableLogin()
    props.history.push('/signup')
  }

  const openProfile = () => {
    props.history.push('/settings')
    closeMenu()
  }

  return (
    <MUIAppBar className={className} {...rest}>
      <Toolbar
        disableGutters
        variant={isXSmall ? 'regular' : 'dense'}
        className={classes.toolbar}
      >
        {Children.count(children) === 0 ? (
          <Typography
            variant="h6"
            color="inherit"
            className={classes.title}
            id="react-admin-title"
          />
        ) : (
          children
        )}
        <LoadingIndicator />
        {authenticated && (
          <div>
            <IconButton
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={openMenu}
              color="inherit"
            >
              <AccountCircle />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorEl}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              open={isMenuOpen}
              onClose={closeMenu}
            >
              <MenuItem onClick={openProfile}>Profile</MenuItem>
              <MenuItem onClick={handleLogout}>Logout</MenuItem>
            </Menu>
          </div>
        )}
        {!authenticated && (
          <div>
            <Button
              variant="outlined"
              color="secondary"
              onClick={handleStartLogin}
              startIcon={<LockOpen />}
              style={{
                color: 'white',
                margin: '0 12px',
                borderColor: 'white',
              }}
            >
              Log In
            </Button>
            <Button
              variant="outlined"
              color="secondary"
              onClick={handleSignup}
              startIcon={<PersonAdd />}
              style={{ color: 'white', borderColor: 'white' }}
            >
              Sign Up
            </Button>
          </div>
        )}
      </Toolbar>
    </MUIAppBar>
  )
}

const AppBar = withRouter(AppBarImpl)
export default AppBar
