import React, { FunctionComponent, useEffect, useMemo } from 'react'
import {
  createMuiTheme,
  makeStyles,
  Theme,
  ThemeProvider,
} from '@material-ui/core/styles'
import classnames from 'classnames'
import { Notification } from 'ra-ui-materialui'
import { Avatar, Card } from '@material-ui/core'
import { Lock } from '@material-ui/icons'
import { useHistory } from 'react-router'
import { useCheckAuth } from 'react-admin'

const useStyles = makeStyles(
  (theme: Theme) => ({
    main: {
      display: 'flex',
      flexDirection: 'column',
      minHeight: '100vh',
      alignItems: 'center',
      justifyContent: 'flex-start',
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover',
      backgroundImage:
        'radial-gradient(circle at 50% 14em, #313264 0%, #00023b 60%, #00023b 100%)',
    },
    card: {
      minWidth: 300,
      marginTop: '6em',
    },
    avatar: {
      margin: '1em',
      display: 'flex',
      justifyContent: 'center',
    },
    lockIcon: {
      backgroundColor: theme.palette.secondary.main,
    },
  }),
  { name: 'RaLoginForm' },
)

export const AuthLayout: FunctionComponent = (props: any) => {
  const {
    theme,
    classes: classesOverride,
    className,
    children,
    staticContext,
    redirectTo,
    backgroundImage,
    ...rest
  } = props
  const classes = useStyles(props)
  const muiTheme = useMemo(() => createMuiTheme(theme), [theme])
  const checkAuth = useCheckAuth()
  const history = useHistory()
  useEffect(() => {
    checkAuth({}, false)
      .then(() => {
        // already authenticated, redirect to the home page
        history.push('/')
      })
      .catch(() => {
        // not authenticated, stay on the login page
      })
  }, [checkAuth, history])

  return (
    <ThemeProvider theme={muiTheme}>
      <div className={classnames(classes.main, className)} {...rest}>
        <Card className={classes.card}>
          <div className={classes.avatar}>
            <Avatar className={classes.lockIcon}>
              <Lock />
            </Avatar>
          </div>
          {children}
        </Card>
        <Notification />
      </div>
    </ThemeProvider>
  )
}
