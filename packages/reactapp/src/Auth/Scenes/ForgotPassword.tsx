import React from 'react'
import { AuthLayout } from './AuthLayout'
import { useState } from 'react'
import { useHistory } from 'react-router'
import { ForgotPasswordForm } from './Forms/ForgotPasswordForm'
import { ConfirmForgetPasswordForm } from './Forms/ConfirmForgetPasswordForm'
import { Box, Link, Typography } from '@material-ui/core'
import { AuthProvider } from '../auth.provider'

export const ForgotPassword = (props: any) => {
  const [email, setEmail] = useState<string>()
  const history = useHistory()

  const onPasswordReset = (email: string) => {
    setEmail(email)
  }

  const onPasswordSet = (result: any) => {
    history.push('/')
  }

  return (
    <AuthLayout>
      <Typography variant="h6" style={{ padding: '0 0.75em' }}>
        Forgot Password?
      </Typography>
      {!email && <ForgotPasswordForm onPasswordReset={onPasswordReset} />}
      {email && (
        <>
          <Typography style={{ padding: '0 0.75em' }}>
            We've sent you a One Time Code to the email {email}. Please enter it
            below.
          </Typography>
          <ConfirmForgetPasswordForm
            email={email}
            onPasswordSet={onPasswordSet}
          />
        </>
      )}
      {!email && <Box p={2} display="flex" justifyContent="center">
        <Link
          component="button"
          variant="body2"
          onClick={() => {
            history.push('/login')
          }}
        >
          Login
        </Link>
      </Box>}
      {!email && <Box p={2} display="flex" justifyContent="center">
        <Link
          component="button"
          variant="body2"
          onClick={() => {
            AuthProvider.disableLogin()
            history.push('/')
          }}
        >
          Open Public Directory
        </Link>
      </Box>}
    </AuthLayout>
  )
}
