import React from 'react'
import { Form } from 'react-final-form'
import CardActions from '@material-ui/core/CardActions'
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'
import { useNotify, useSafeSetState } from 'ra-core'
import { AuthProvider } from '../../auth.provider'
import { useFormStyles } from './formStyles'
import { PasswordNotSecureMessage, PasswordRegexp } from './passwordValidation'
import { TextField } from 'mui-rff'

interface FormData {
  code: string
  password: string
}

interface ConfirmForgetPasswordFormProps {
  email: string
  onPasswordSet: (result: any) => void
}

export const ConfirmForgetPasswordForm = ({
  email,
  onPasswordSet,
  ...props
}: ConfirmForgetPasswordFormProps) => {
  const classes = useFormStyles(props)
  const [loading, setLoading] = useSafeSetState<boolean>(false)
  const notify = useNotify()
  const validate = (values: FormData) => {
    const errors: any = {}
    if (!values.code) {
      errors.password = 'Please enter verification code from the email.'
    }
    if (!values.password) {
      errors.password = 'Please enter your password.'
    }
    if (!PasswordRegexp.test(values.password)) {
      errors.password = PasswordNotSecureMessage
    }
    return errors
  }

  const submit = ({ password, code }: FormData) => {
    setLoading(true)
    AuthProvider.confirmForgotPassword({
      email,
      password,
      code,
    })
      .then(result => {
        setLoading(false)
        onPasswordSet(result)
      })
      .catch(error => {
        setLoading(false)
        notify(
          typeof error === 'string'
            ? error
            : typeof error === 'undefined' || !error.message
            ? 'Something went wrong.'
            : error.message,
          'warning',
        )
      })
  }

  return (
    <Form
      onSubmit={submit}
      validate={validate}
      render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit} noValidate>
          <div className={classes.form}>
            <div className={classes.input}>
              <TextField
                autoFocus
                id="code"
                name="code"
                label="Verification Code"
                disabled={loading}
              />
            </div>
            <div className={classes.input}>
              <TextField
                autoFocus
                id="password"
                name="password"
                type="password"
                label="New Password"
                disabled={loading}
              />
            </div>
          </div>
          <CardActions>
            <Button
              variant="contained"
              type="submit"
              color="primary"
              disabled={loading}
              className={classes.button}
            >
              {loading && (
                <CircularProgress
                  className={classes.icon}
                  size={18}
                  thickness={2}
                />
              )}
              Save New Password
            </Button>
          </CardActions>
        </form>
      )}
    />
  )
}
