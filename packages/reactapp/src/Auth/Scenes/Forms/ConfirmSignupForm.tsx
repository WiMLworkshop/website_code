import React from 'react'
import { Form } from 'react-final-form'
import CardActions from '@material-ui/core/CardActions'
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'
import { useNotify, useSafeSetState } from 'ra-core'
import { AuthProvider } from '../../auth.provider'
import { useFormStyles } from './formStyles'
import { TextField } from 'mui-rff'
import { Typography } from '@material-ui/core'

interface FormData {
  code: string
}

interface ConfirmSignupFormProps {
  email: string
  password: string
  onSignupConfirmed: (result: any) => void
}

export const ConfirmSignupForm = ({
  email,
  password,
  onSignupConfirmed,
  ...props
}: ConfirmSignupFormProps) => {
  const classes = useFormStyles(props)
  const [loading, setLoading] = useSafeSetState<boolean>(false)
  const notify = useNotify()
  const validate = (values: FormData) => {
    const errors: any = {}
    if (!values.code) {
      errors.password = 'Please enter your code from the email.'
    }
    return errors
  }

  const submit = ({ code }: FormData) => {
    setLoading(true)
    AuthProvider.confirmSignUp({
      email,
      password,
      code,
    })
      .then(result => {
        setLoading(false)
        onSignupConfirmed(result)
      })
      .catch(error => {
        setLoading(false)
        notify(
          typeof error === 'string'
            ? error
            : typeof error === 'undefined' || !error.message
            ? 'Something went wrong.'
            : error.message,
          'warning',
        )
      })
  }

  return (
    <Form
      onSubmit={submit}
      validate={validate}
      render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit} noValidate>
          <div className={classes.form}>
            <Typography>
              Please check your email for verification code.
            </Typography>
            <div className={classes.input}>
              <TextField
                autoFocus
                id="code"
                name="code"
                label="Verification Code"
                disabled={loading}
              />
            </div>
          </div>
          <CardActions>
            <Button
              variant="contained"
              type="submit"
              color="primary"
              disabled={loading}
              className={classes.button}
            >
              {loading && (
                <CircularProgress
                  className={classes.icon}
                  size={18}
                  thickness={2}
                />
              )}
              Confirm
            </Button>
          </CardActions>
        </form>
      )}
    />
  )
}
