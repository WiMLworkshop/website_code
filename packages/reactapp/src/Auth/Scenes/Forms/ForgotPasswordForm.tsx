import React from 'react'
import { Form } from 'react-final-form'
import CardActions from '@material-ui/core/CardActions'
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'
import { useNotify, useSafeSetState } from 'ra-core'
import { AuthProvider } from '../../auth.provider'
import { useFormStyles } from './formStyles'
import { TextField } from 'mui-rff'

interface FormData {
  email: string
}

interface ForgotPasswordFormProps {
  onPasswordReset: (email: string) => void
}

export const ForgotPasswordForm = ({
  onPasswordReset,
  ...props
}: ForgotPasswordFormProps) => {
  const classes = useFormStyles(props)
  const [loading, setLoading] = useSafeSetState<boolean>(false)
  const notify = useNotify()
  const validate = (values: FormData) => {
    const errors: any = {}
    if (!values.email) {
      errors.password = 'Please enter your email.'
    }
    return errors
  }

  const submit = ({ email }: FormData) => {
    setLoading(true)
    AuthProvider.forgotPassword({ email })
      .then(result => {
        setLoading(false)
        onPasswordReset(email)
      })
      .catch(error => {
        setLoading(false)
        notify(
          typeof error === 'string'
            ? error
            : typeof error === 'undefined' || !error.message
            ? 'Something went wrong.'
            : error.message,
          'warning',
        )
      })
  }

  return (
    <Form
      onSubmit={submit}
      validate={validate}
      render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit} noValidate>
          <div className={classes.form}>
            <div className={classes.input}>
              <TextField
                autoFocus
                id="email"
                name="email"
                type="email"
                label="Your Email"
                disabled={loading}
              />
            </div>
          </div>
          <CardActions>
            <Button
              variant="contained"
              type="submit"
              color="primary"
              disabled={loading}
              className={classes.button}
            >
              {loading && (
                <CircularProgress
                  className={classes.icon}
                  size={18}
                  thickness={2}
                />
              )}
              Reset Password
            </Button>
          </CardActions>
        </form>
      )}
    />
  )
}
