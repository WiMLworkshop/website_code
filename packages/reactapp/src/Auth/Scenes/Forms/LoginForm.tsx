import React from 'react'
import { Form } from 'react-final-form'
import CardActions from '@material-ui/core/CardActions'
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'
import { useNotify, useSafeSetState } from 'ra-core'
import { AuthProvider } from '../../auth.provider'
import { useFormStyles } from './formStyles'
import { TextField } from 'mui-rff'

interface FormData {
  email: string
  password: string
}

interface LoginFormProps {
  onLogin: (email: string, password: string, result: any) => void
}

export const LoginForm = ({ onLogin, ...props }: LoginFormProps) => {
  const classes = useFormStyles(props)
  const [loading, setLoading] = useSafeSetState<boolean>(false)
  const notify = useNotify()
  const validate = (values: FormData) => {
    const errors: any = {}

    if (!values.email) {
      errors.email = 'Please enter your email.'
    }
    if (!values.password) {
      errors.password = 'Please enter your password.'
    }
    return errors
  }

  const submit = (values: FormData) => {
    setLoading(true)
    AuthProvider.login(values)
      .then(result => {
        setLoading(false)
        onLogin(values.email, values.password, result)
      })
      .catch(error => {
        setLoading(false)
        notify(
          typeof error === 'string'
            ? error
            : typeof error === 'undefined' || !error.message
            ? 'Something went wrong.'
            : error.message,
          'warning',
        )
      })
  }

  return (
    <Form
      onSubmit={submit}
      validate={validate}
      render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit} noValidate>
          <div className={classes.form}>
            <div className={classes.input}>
              <TextField
                autoFocus
                id="email"
                name="email"
                type="email"
                label="Email"
                disabled={loading}
              />
            </div>
            <div className={classes.input}>
              <TextField
                id="password"
                name="password"
                label="Password"
                type="password"
                disabled={loading}
                autoComplete="current-password"
              />
            </div>
          </div>
          <CardActions>
            <Button
              variant="contained"
              type="submit"
              color="primary"
              disabled={loading}
              className={classes.button}
            >
              {loading && (
                <CircularProgress
                  className={classes.icon}
                  size={18}
                  thickness={2}
                />
              )}
              Log In
            </Button>
          </CardActions>
        </form>
      )}
    />
  )
}
