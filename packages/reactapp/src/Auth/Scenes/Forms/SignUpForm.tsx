import React from 'react'
import { Form } from 'react-final-form'
import CardActions from '@material-ui/core/CardActions'
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'
import { useNotify, useSafeSetState } from 'ra-core'
import {SelectArrayInput, useQueryWithStore} from 'react-admin'
import { AuthProvider } from '../../auth.provider'
import { useFormStyles } from './formStyles'
import { PasswordNotSecureMessage, PasswordRegexp } from './passwordValidation'
import { TextField, Select, Checkboxes } from 'mui-rff'
import { RECAPTCHA_SITE_KEY } from '../../../constants'
import ReCAPTCHA from 'react-google-recaptcha'
import {Typography} from '@material-ui/core'

interface FormData {
  email: string
  password: string
  firstName: string
  lastName: string
  positionTypeId: number
  fields: number[]
  affiliation: string
  public: boolean
  mentor: boolean
  informAboutOpportunities: boolean
  informAboutActivities: boolean
  city: string
  countryId: number
  keywords?: string
  website?: string
  captcha: string
}

interface SignupFormProps {
  onSignup: (email: string, password: string, result: any) => void
}

export const SignupForm = ({ onSignup, ...props }: SignupFormProps) => {
  const classes = useFormStyles(props)
  const [loadingForm, setLoading] = useSafeSetState<boolean>(false)
  const notify = useNotify()
  const getCountries = useQueryWithStore({
    type: 'getList',
    resource: 'countries',
    payload: {
      pagination: {
        page: 1,
        perPage: 500,
      },
      sort: {
        field: 'name',
        order: 'ASC',
      },
      filter: {},
    },
  })
  const getFieldsOfResearch = useQueryWithStore({
    type: 'getList',
    resource: 'research-fields',
    payload: {
      pagination: {
        page: 1,
        perPage: 500,
      },
      sort: {
        field: 'name',
        order: 'ASC',
      },
      filter: {},
    },
  })
  const getPositionTypes = useQueryWithStore({
    type: 'getList',
    resource: 'positions',
    payload: {
      pagination: {
        page: 1,
        perPage: 500,
      },
      sort: {
        field: 'name',
        order: 'ASC',
      },
      filter: {},
    },
  })
  const validate = (values: FormData) => {
    const errors: any = {}
    if (!values.email) {
      errors.email = 'Please enter your email.'
    }
    if (!values.password) {
      errors.password = 'Please enter your password.'
    }
    if (!PasswordRegexp.test(values.password)) {
      errors.password = PasswordNotSecureMessage
    }
    if (!values.firstName) {
      errors.firstName = 'Please enter your first name.'
    }
    if (!values.lastName) {
      errors.lastName = 'Please enter your last name.'
    }
    if (!values.positionTypeId) {
      errors.positionTypeId = 'Please select your position at the affiliation.'
    }
    if (!values.fields) {
      errors.fields = 'Please select your field of work.'
    }
    if (!values.affiliation) {
      errors.affiliation = 'Please enter your affiliation.'
    }
    if (!values.city) {
      errors.city = 'Please enter your current city.'
    }
    if (!values.countryId) {
      errors.countryId = 'Please select your current country.'
    }
    if (values && values.keywords && values.keywords.length > 512) {
      errors.keywords = 'Keywords are 512 characters max.'
    }
    if (!values.captcha) {
      errors.captcha = 'Please check captcha'
    }
    return errors
  }

  const submit = (data: FormData) => {
    setLoading(true)
    AuthProvider.signUp(data)
      .then(result => {
        setLoading(false)
        onSignup(data.email, data.password, result)
      })
      .catch(error => {
        setLoading(false)
        notify(
          typeof error === 'string'
            ? error
            : typeof error === 'undefined' || !error.message
            ? 'Something went wrong.'
            : error.message,
          'warning',
        )
      })
  }

  return (
    <Form
      onSubmit={submit}
      validate={validate}
      render={({ handleSubmit, form }) => {
        return (
          <form onSubmit={handleSubmit} noValidate>
            <div className={classes.form}>
              <div className={classes.input}>
                <TextField
                  autoFocus
                  name="email"
                  label="Email"
                  required
                  helperText="We will never post this publicly. Your email is your username for login and account verification. It can be used for contact based on your notification preferences (see below)."
                  disabled={loadingForm}
                />
              </div>
              <div className={classes.input}>
                <TextField
                  name="password"
                  type="password"
                  label="Password"
                  required
                  disabled={loadingForm}
                />
              </div>
              <div className={classes.input}>
                <TextField
                  name="firstName"
                  label="First Name"
                  required
                  disabled={loadingForm}
                />
              </div>
              <div className={classes.input}>
                <TextField
                  name="lastName"
                  label="Last Name"
                  required
                  disabled={loadingForm}
                />
              </div>
              <div className={classes.input}>
                <Select
                  label="Position Type / Title"
                  name="positionTypeId"
                  required
                  disabled={loadingForm || getPositionTypes.loading}
                  helperText={'Your current position or title, as is approximately reflected in this list.'}
                  data={
                    getPositionTypes.data
                      ? getPositionTypes.data.map((c: any) => ({ label: c.name, value: c.id }))
                      : []
                  }
                />
              </div>
              <div className={classes.input}>
                <SelectArrayInput
                  label="Field of Research"
                  name="fields"
                  required
                  helperText={'Select the most relevant topics to your research.'}
                  disabled={loadingForm || getFieldsOfResearch.loading}
                  fullWidth
                  style={{ minWidth: 800 }}
                  choices={
                    getFieldsOfResearch.data
                      ? getFieldsOfResearch.data
                      : []
                  }
                />
              </div>
              <div className={classes.input}>
                <TextField
                  id="affiliation"
                  name="affiliation"
                  label="Affiliation"
                  required
                  disabled={loadingForm}
                  helperText={'Your institution or company.'}
                />
              </div>
              <div className={classes.input}>
                <TextField
                  name="city"
                  label="City"
                  required
                  disabled={loadingForm}
                  helperText={'The city of your current institution/place of work.'}
                />
              </div>
              <div className={classes.input}>
                <Select
                  label="Country"
                  name="countryId"
                  required
                  disabled={loadingForm || getCountries.loading}
                  helperText={'The country of your current institution/place of work.'}
                  data={
                    getCountries.data
                      ? getCountries.data.map((c: any) => ({ label: c.name, value: c.id }))
                      : []
                  }
                />
              </div>
              <div className={classes.input}>
                <TextField
                  name="website"
                  label="Website"
                  helperText={
                    'An external link (webpage, institutional page, LinkedIn, ...) so that users can contact you and find out more about you.'
                  }
                  disabled={loadingForm}
                />
              </div>
              <div className={classes.input}>
                <TextField
                  name="keywords"
                  label="Keywords"
                  helperText={
                    'Please write a few comma separated words about the specifics of your research if not represented in the Field of Research (e.g. "Medical imaging, human-in-the-loop", max 500 characters).'
                  }
                  disabled={loadingForm}
                />
              </div>
              <div className={classes.input}>
                <Checkboxes
                  name="public"
                  data={{
                    label: 'I would like to appear in the public directory.',
                    value: false,
                  }}
                  disabled={loadingForm}
                />
              </div>
              <div className={classes.input}>
                <Typography>I agree to be contacted by WiML about:</Typography>
                <div className={classes.input}>
                  <Checkboxes
                    name="informAboutActivities"
                    data={{
                      label:
                        'upcoming WiML activities',
                      value: false,
                    }}
                    disabled={loadingForm}
                  />
                </div>
                <div className={classes.input}>
                  <Checkboxes
                    name="mentor"
                    data={{
                      label: 'upcoming WiML calls for volunteers/mentors',
                      value: false,
                    }}
                    disabled={loadingForm}
                  />
                </div>
                <div className={classes.input}>
                  <Checkboxes
                    name="informAboutOpportunities"
                    data={{
                      label:
                        'opportunities at WiML partners or events organized by WiML partners',
                      value: false,
                    }}
                    disabled={loadingForm}
                  />
                </div>
              </div>
              <div className={classes.input}>
                <ReCAPTCHA
                  onChange={token => form.change('captcha', token)}
                  sitekey={RECAPTCHA_SITE_KEY}
                />
              </div>
            </div>
            <CardActions>
              <Button
                variant="contained"
                type="submit"
                color="primary"
                disabled={loadingForm}
                className={classes.button}
              >
                {loadingForm && (
                  <CircularProgress
                    className={classes.icon}
                    size={18}
                    thickness={2}
                  />
                )}
                Sign Up
              </Button>
            </CardActions>
          </form>
        )
      }}
    />
  )
}
