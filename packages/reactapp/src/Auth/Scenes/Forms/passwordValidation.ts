export const PasswordRegexp = new RegExp(
  /^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/,
)
export const PasswordNotSecureMessage =
  'Password must contain at least 8 characters, one uppercase, one lowercase, \none number and one special character'
