import React from 'react'
import { AuthLayout } from './AuthLayout'
import { useState } from 'react'
import { LoginForm } from './Forms/LoginForm'
import { useHistory } from 'react-router'
import { SetNewPasswordForm } from './Forms/SetNewPasswordForm'
import { ConfirmSignupForm } from './Forms/ConfirmSignupForm'
import { Box, Link, Typography } from '@material-ui/core'
import { AuthProvider } from '../auth.provider'

interface State {
  email?: string
  password?: string
  challengeSession?: string
  requiresConfirmation?: boolean
  user?: any
}

export const Login = (props: any) => {
  const [
    { email, password, challengeSession, requiresConfirmation, user, },
    setState,
  ] = useState<State>({})
  const history = useHistory()

  const onLogin = (email: string, password: string, result: any) => {
    if (result.session && result.session.accessToken) {
      history.push('/')
    } else {
      setState({
        email,
        password,
        challengeSession:
          result.challenge && result.challenge.session
            ? result.challenge.session
            : undefined,
        requiresConfirmation:
          result.challenge &&
          result.challenge.challengeName === 'SIGN_UP_CONFIRMATION_REQUIRED',
        user: result.user,
      })
    }
  }

  const onPasswordSet = (result: any) => {
    history.push('/')
  }

  return (
    <AuthLayout>
      <Typography variant="h6" style={{ padding: '0 0.75em' }}>
        Log In
      </Typography>
      {!email && !password && <LoginForm onLogin={onLogin} />}
      {email && challengeSession && (
        <SetNewPasswordForm
          email={email}
          user={user}
          challengeSesssion={challengeSession}
          onPasswordSet={onPasswordSet}
        />
      )}
      {email && password && requiresConfirmation === true && (
        <ConfirmSignupForm
          email={email}
          password={password}
          onSignupConfirmed={onPasswordSet}
        />
      )}
      {!email && !password && <Box p={2} display="flex" justifyContent="center">
        <Link
          component="button"
          variant="body2"
          onClick={() => {
            history.push('/forgot-password')
          }}
        >
          Forgot Password
        </Link>
      </Box>}
      {!email && !password && <Box p={2} display="flex" justifyContent="center">
        <Link
          component="button"
          variant="body2"
          onClick={() => {
            AuthProvider.disableLogin()
            history.push('/')
          }}
        >
          Open Public Directory
        </Link>
      </Box>}
    </AuthLayout>
  )
}
