import React from 'react'
import { AuthLayout } from './AuthLayout'
import { useState } from 'react'
import { useHistory } from 'react-router'
import { ConfirmSignupForm } from './Forms/ConfirmSignupForm'
import { SignupForm } from './Forms/SignUpForm'
import { Box, Link, Typography } from '@material-ui/core'
import { AuthProvider } from '../auth.provider'

interface State {
  email?: string
  password?: string
}

export const Signup = (props: any) => {
  const [{ email, password }, setState] = useState<State>({})
  const history = useHistory()

  const onSignup = (email: string, password: string, result: any) => {
    setState({
      email,
      password,
    })
  }

  const onPasswordSet = (result: any) => {
    history.push('/')
  }

  return (
    <AuthLayout>
      <Typography variant="h6" style={{ padding: '0 0.75em' }}>
        Sign Up
      </Typography>
      {!email && !password && <SignupForm onSignup={onSignup} />}
      {email && password && (
        <ConfirmSignupForm
          email={email}
          password={password}
          onSignupConfirmed={onPasswordSet}
        />
      )}
      {!email && !password && <Box p={2} display="flex" justifyContent="center">
        <Link
          component="button"
          variant="body2"
          onClick={() => {
            history.push('/forgot-password')
          }}
        >
          Forgot Password
        </Link>
      </Box>}
      {!email && !password && <Box p={2} display="flex" justifyContent="center">
        <Link
          component="button"
          variant="body2"
          onClick={() => {
            AuthProvider.disableLogin()
            history.push('/')
          }}
        >
          Open Public Directory
        </Link>
      </Box>}
    </AuthLayout>
  )
}
