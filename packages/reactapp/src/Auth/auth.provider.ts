import { API_URL } from '../constants'
import { fetchUtils } from 'react-admin'

export enum PermissionsLevel {
  Guest = 'Guest',
  User = 'User',
  Admin = 'Admin',
}

const LOGIN_URL = `${API_URL}/auth/login`
const SET_NEW_PASSWORD_URL = `${API_URL}/auth/set-password`
const REFRESH_TOKEN_URL = `${API_URL}/auth/refresh`
const FORGOT_PASSWORD_URL = `${API_URL}/auth/reset`
const CONFIRM_FORGOT_PASSWORD_URL = `${API_URL}/auth/confirm-reset`
const SIGNUP_URL = `${API_URL}/auth/signup`
const CONFIRM_SIGNUP_URL = `${API_URL}/auth/confirm-signup`
const LOGOUT_URL = `${API_URL}/auth/logout`

export const KEYS = {
  Token: 'token',
  RefreshToken: 'refresh_token',
  Role: 'role',
  RequireLogin: 'require_login',
}

if (!localStorage.getItem(KEYS.Token)) {
  localStorage.removeItem(KEYS.RequireLogin)
}

export const httpClient = (url: string, options: any) => {
  if (!options.headers) {
    options.headers = new Headers({ Accept: 'application/json' })
  }
  const token = localStorage.getItem(KEYS.Token)
  if (token) {
    options.headers.set('Authorization', `Bearer ${token}`)
    options.withCredentials = true
    options.credentials = 'include'
  }
  return fetchUtils.fetchJson(url, options)
}

if (!localStorage.getItem(KEYS.Role)) {
  localStorage.setItem(KEYS.Role, PermissionsLevel.Guest)
}

export const AuthProvider = {
  login: ({ email, password }: any) => {
    const request = new Request(LOGIN_URL, {
      method: 'POST',
      body: JSON.stringify({ email, password }),
      headers: new Headers({ 'Content-Type': 'application/json' }),
    })
    return fetch(request)
      .then(AuthProvider.rejectIfUnsuccessful)
      .then(AuthProvider.serializeJson)
      .then(AuthProvider.processLoginResponse)
  },

  enableLogin: () => {
    localStorage.setItem(KEYS.RequireLogin, 'true')
  },

  disableLogin: () => {
    localStorage.removeItem(KEYS.RequireLogin)
  },

  submitNewPassword: (data: any) => {
    const request = new Request(SET_NEW_PASSWORD_URL, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: new Headers({ 'Content-Type': 'application/json' }),
    })
    return fetch(request)
      .then(AuthProvider.rejectIfUnsuccessful)
      .then(AuthProvider.serializeJson)
      .then(AuthProvider.processLoginResponse)
  },

  forgotPassword: ({ email }: any) => {
    const request = new Request(FORGOT_PASSWORD_URL, {
      method: 'POST',
      body: JSON.stringify({ email }),
      headers: new Headers({ 'Content-Type': 'application/json' }),
    })
    return fetch(request).then(AuthProvider.rejectIfUnsuccessful)
  },

  confirmForgotPassword: ({ email, password, code }: any) => {
    const request = new Request(CONFIRM_FORGOT_PASSWORD_URL, {
      method: 'POST',
      body: JSON.stringify({ email, password, code }),
      headers: new Headers({ 'Content-Type': 'application/json' }),
    })
    return fetch(request)
      .then(AuthProvider.rejectIfUnsuccessful)
      .then(AuthProvider.serializeJson)
      .then(AuthProvider.processLoginResponse)
  },

  signUp: (data: any) => {
    const request = new Request(SIGNUP_URL, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: new Headers({ 'Content-Type': 'application/json' }),
    })
    return fetch(request).then(AuthProvider.rejectIfUnsuccessful)
  },

  confirmSignUp: ({ email, password, code }: any) => {
    const request = new Request(CONFIRM_SIGNUP_URL, {
      method: 'POST',
      body: JSON.stringify({ email, password, code }),
      headers: new Headers({ 'Content-Type': 'application/json' }),
    })
    return fetch(request)
      .then(AuthProvider.rejectIfUnsuccessful)
      .then(AuthProvider.serializeJson)
      .then(AuthProvider.processLoginResponse)
  },

  processLoginResponse: (data: any) => {
    if (data.session) {
      localStorage.setItem(KEYS.Token, data.session.accessToken)
      localStorage.setItem(KEYS.RefreshToken, data.session.refreshToken)
      localStorage.setItem(KEYS.Role, data.role)
      localStorage.removeItem(KEYS.RequireLogin)
    }
    return Promise.resolve(data)
  },

  rejectIfUnsuccessful: (response: any) => {
    if (response.status < 200 || response.status >= 300) {
      return Promise.reject(new Error(response.statusText))
    }
    return Promise.resolve(response)
  },

  serializeJson: (response: any) => {
    return Promise.resolve(response.json())
  },

  refreshToken: () => {
    const refreshToken = localStorage.getItem(KEYS.RefreshToken)
    if (!refreshToken) {
      return Promise.reject()
    }
    const request = new Request(REFRESH_TOKEN_URL, {
      method: 'POST',
      body: JSON.stringify({ refreshToken }),
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
    })
    return fetch(request)
      .then(AuthProvider.rejectIfUnsuccessful)
      .then(AuthProvider.serializeJson)
      .then(({ accessToken }) => {
        localStorage.setItem(KEYS.Token, accessToken)
      })
  },

  logout: () => {
    const token = localStorage.getItem(KEYS.Token)
    if (token) {
      const request = new Request(LOGOUT_URL, {
        method: 'POST',
        headers: new Headers({
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        }),
        credentials: 'include',
        mode: 'cors',
      })
      fetch(request)
    }
    localStorage.removeItem(KEYS.Token)
    localStorage.removeItem(KEYS.RefreshToken)
    localStorage.removeItem(KEYS.Role)
    localStorage.setItem(KEYS.Role, PermissionsLevel.Guest)
    return Promise.resolve()
  },

  checkError: (error: any) => {
    const status = error.status
    if (status === 401 || status === 403) {
      return AuthProvider.refreshToken()
    }
    return Promise.resolve()
  },

  checkAuth: () => {
    return localStorage.getItem(KEYS.RequireLogin)
      ? Promise.reject()
      : Promise.resolve()
  },

  getPermissions: () => Promise.resolve(localStorage.getItem(KEYS.Role)),
}
