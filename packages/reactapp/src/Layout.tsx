import React from 'react'
import { Layout as RALayout } from 'react-admin'
import AppBar from './AppBar'

const Layout = (props: any) => (
  <RALayout {...props} open={false} appBar={AppBar} />
)

export default Layout
