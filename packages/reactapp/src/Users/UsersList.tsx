import * as React from 'react'
import {
  List,
  Datagrid,
  TextField,
  TextInput,
  Filter,
  BooleanInput,
  SelectInput,
  ReferenceInput,
  SimpleList,
  TopToolbar,
  ExportButton,
  sanitizeListRestProps,
  ReferenceArrayInput,
  ArrayField,
  SingleFieldList,
  SelectArrayInput,
  ChipField,
} from 'react-admin'
import { PermissionsLevel } from '../Auth'
import { Link, useMediaQuery } from '@material-ui/core'
import { Link as LinkIcon } from '@material-ui/icons'
import {cloneElement} from 'react'

const UserFilter = ({ permissions, ...props }: any) => (
  <Filter {...props}>
    <ReferenceArrayInput
      label="Position Type / Title"
      source="positionTypeId||eq"
      reference="positions"
      style={{ minWidth: 200 }}
      alwaysOn
    >
      <SelectArrayInput optionText="name" />
    </ReferenceArrayInput>
    <ReferenceArrayInput
      label="Fields Of Research"
      source="fields.id||eq"
      reference="research-fields"
      style={{ minWidth: 200 }}
      alwaysOn
    >
      <SelectArrayInput optionText="name" />
    </ReferenceArrayInput>
    <ReferenceInput
      label="Country"
      source="countryId"
      reference="countries"
      perPage={500}
      sort={{ field: 'name', order: 'ASC' }}
    >
      <SelectInput optionText="name" />
    </ReferenceInput>
    <TextInput label="Affiliation" source="affiliation||cont" />
    <SelectInput
      label="Website Listed"
      source="website"
      choices={[
        { id: 'starts||http', name: 'Yes' },
        { id: 'excl||http', name: 'No' },
      ]}
    />
    <TextInput label="City" source="city||cont" />
    <TextInput label="Keywords" source="keywords||cont" />
    {permissions === PermissionsLevel.Admin && (
      <ReferenceInput label="Role" source="role" reference="roles">
        <SelectInput optionText="name" />
      </ReferenceInput>
    )}
    {permissions === PermissionsLevel.Admin && (
      <BooleanInput label="Listed Publicly" source="public||eq" />
    )}
    {permissions === PermissionsLevel.Admin && (
      <BooleanInput label="Mentor Emails" source="mentor||eq" />
    )}
    {permissions === PermissionsLevel.Admin && (
      <BooleanInput label="Opportunities Emails" source="informAboutOpportunities||eq" />
    )}
    {permissions === PermissionsLevel.Admin && (
      <BooleanInput label="Activities Emails" source="informAboutActivities||eq" />
    )}
    {permissions === PermissionsLevel.Admin && (
      <BooleanInput label="Verified Email" source="verified||eq" />
    )}
  </Filter>
)

const FullNameField = ({ record }: any) => <span>{record.firstName} {record.lastName}</span>
FullNameField.defaultProps = { label: 'Full Name' }

const WebsiteField = ({ record }: any) => (<Link
  target="_blank"
  rel="noopener"
  href={record.website}
>
  {record.website ? <LinkIcon /> : ''}
</Link>)
WebsiteField.defaultProps = { label: 'Website' }

const ListActions = ({
   currentSort,
   className,
   resource,
   filters,
   displayedFilters,
   exporter, // you can hide ExportButton if exporter = (null || false)
   filterValues,
   permanentFilter,
   hasCreate, // you can hide CreateButton if hasCreate = false
   basePath,
   selectedIds,
   onUnselectItems,
   showFilter,
   maxResults,
   total,
   permissions,
   ...rest
 }: any) => (
  <TopToolbar className={className} {...sanitizeListRestProps(rest)}>
    {filters && cloneElement(filters, {
      resource,
      showFilter,
      displayedFilters,
      filterValues,
      context: 'button',
    })}
    {permissions === PermissionsLevel.Admin && <ExportButton
        disabled={total === 0}
        resource={resource}
        sort={currentSort}
        filter={{ ...filterValues, ...permanentFilter }}
        exporter={exporter}
        maxResults={maxResults}
    />}
  </TopToolbar>
)

export const UsersList = ({ permissions, ...props }: any) => {
  const isSmall = useMediaQuery((theme: any) => theme.breakpoints.down('sm'))
  return (
    <List
      hasCreate={false}
      hasEdit={permissions === PermissionsLevel.Admin}
      hasList={true}
      actions={<ListActions permissions={permissions} {...props} />}
      hasShow={false}
      {...props}
      bulkActionButtons={false}
      filters={<UserFilter permissions={permissions} {...props} />}
      title="Directory of Women in Machine Learning"
      perPage={25}
    >
      {isSmall ? (
        <SimpleList
          primaryText={(record: any) =>
            `${record.firstName} ${record.lastName}`
          }
          secondaryText={(record: any) => `${record.positionType ? record.positionType.name : 'Unknown' }`}
          tertiaryText={(record: any) => `${record.affiliation}`}
          linkType={'show'}
        />
      ) : (
        <Datagrid
          rowClick={permissions === PermissionsLevel.Admin ? 'edit' : undefined}
        >
          <FullNameField source="firstName" />
          <TextField label="Position Type / Title" source="positionType.name" />
          <TextField source="affiliation" />
          <WebsiteField source="website" />
          <ArrayField label="Field Of Research" source="fields">
            <SingleFieldList linkType={false}>
              <ChipField source="name" />
            </SingleFieldList>
          </ArrayField>
          <TextField
            noWrap
            style={{ display: 'inline-block', maxWidth: 300 }}
            source="keywords"
          />
        </Datagrid>
      )}
    </List>
  )
}

