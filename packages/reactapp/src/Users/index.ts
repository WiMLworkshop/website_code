import { PermissionsLevel } from '../Auth'
import { UserEdit } from './UserEdit'
import { ShowGuesser } from 'react-admin'

export default function(role: PermissionsLevel) {
  return {
    options: {
      label: 'Member Directory',
    },
    edit: role === PermissionsLevel.Admin ? UserEdit : undefined,
    show: role === PermissionsLevel.Admin ? ShowGuesser : undefined,
  }
}
