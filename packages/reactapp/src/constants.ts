export const API_URL = process.env.REACT_APP_API_URL!
export const RECAPTCHA_SITE_KEY = process.env.REACT_APP_RECAPTCHA_SITE_KEY!
