import React from 'react'
import { Authenticated, WithPermissions } from 'react-admin'
import { Route } from 'react-router-dom'
import {ForgotPassword, Login, PermissionsLevel, Signup} from './Auth'
import { UsersList } from './Users/UsersList'
import { ProfileEdit } from './Users/UserEdit'

export default [
  <Route exact path="/forgot-password" component={ForgotPassword} noLayout />,
  <Route exact path="/signup" component={Signup} noLayout />,
  <Route exact path="/login" component={Login} noLayout />,
  <Route
    exact
    path="/"
    component={({ location, match }: any) => (
      <WithPermissions
        authParams={{}}
        location={location}
        render={({ permissions }: any) => (
          <UsersList
            title={'Member Directory'}
            resource={
              permissions === PermissionsLevel.Admin
                ? 'private/users'
                : 'public/users'
            }
            basePath={
              permissions === PermissionsLevel.Admin
                ? 'private/users'
                : 'public/users'
            }
            permissions={permissions}
          />
        )}
      />
    )}
  />,
  <Route
    exact
    path="/settings"
    component={() => (
      <Authenticated>
        <ProfileEdit />
      </Authenticated>
    )}
  />,
]
